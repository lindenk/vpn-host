# VPN Host deployment tool #

This tool helps provision a vpn server on a remote host and includes utilities to create and manage client certs. Also includes a ReST api for create certs over a network on the fly and easy to manage mounting of an external device to store certs.

# Setup #

This repo should be used as a git submodule but can be used standallow.

First, clone this repo into your vpn deployment project folder.

From within this repositories folder, install deps using `conda env create -f environment.yml`.

Activate the virtual environment using `source activate vpn-host`

Run the setup: `inv setup`

Return to your project directory: `cd ..`.

# Available commands #

Run `inv --list` to list availble commands.