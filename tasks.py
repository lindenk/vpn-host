#!/usr/bin/env python3

from invoke import task
import subprocess, sys, os, yaml, jinja2

current_dir = os.path.dirname(os.path.realpath(__file__))

@task(help={
  "path": "Path to directory to set up environment in. Defaults to .."
})
def setup(c, path=os.path.join(current_dir, '..')):
  """
  Create a standard vpn host layout with vars file and whatnot in the given directory.
  """

  # Symlink invoke file
  c.run("ln -s {} {}".format(os.path.realpath(__file__), os.path.join(path, "tasks.py")))

  # Copy gitignore
  c.run("cp setup/gitignore {}".format(os.path.join(path, ".gitignore")))

  # Copy example config
  c.run("cp setup/config.yml {}".format(os.path.join(path, "config.yml")))

  # Create certs directory
  c.run("mkdir -p {}".format(os.path.join(path, "certs")))

  # Download easyrsa
  print("Downloading easyrsa...")
  c.run("mkdir -p {}".format(os.path.join(path, "tools", "easyrsa")))

  c.run('wget -qO- "https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.4/EasyRSA-3.0.4.tgz" | tar xvz -C {} --strip-components=1'.format(os.path.join(path, "tools", "easyrsa")))

  print("Setup complete. You may want to mount a device on certs to store certs externally")

@task
def clean(c):
  """
  Cleans all setup files from the directory
  """
  if os.path.islink('tasks.py'):
    os.unlink('tasks.py')

  os.remove('tools')

@task(help={
  "domain": "Name of the domain to create certs under. Defaults to 'default'"
})
def init(c, domain='default'):
  """
  Creates a new ca and params under the given domain
  """
  easyrsa_bin = os.path.join(os.path.abspath(os.curdir), "tools", "easyrsa", "easyrsa")

  cert_path = os.path.join("certs", domain)
  c.run("mkdir {}".format(cert_path))
  os.chdir(cert_path)

  print("Generating Static TLS key...")
  c.run("openvpn --genkey --secret {}".format("ta.key"))

  print("Initializing pki...")
  c.run(" ".join(["ln -s", os.path.join(os.path.dirname(easyrsa_bin), "x509-types"), "x509-types"]))
  c.run(" ".join(["ln -s", os.path.join(os.path.dirname(easyrsa_bin), "openssl-easyrsa.cnf"), "openssl-easyrsa.cnf"]))
  c.run(" ".join([easyrsa_bin, "--batch init-pki"]))

  print("Creating CA...")
  c.run(" ".join([easyrsa_bin, "--batch build-ca nopass"]))

  print("Generating DH params...")
  c.run("openssl dhparam -out dh.pem 2048")

@task(help={
  "name": "Name of the server cert",
  "domain": "Domain to create the cert under"
})
def generate_server(c, name='server', domain='default'):
  """
  Generates a server cert with the given name for the given domain.
  """
  
  easyrsa_bin = os.path.join(os.path.abspath(os.curdir), "tools", "easyrsa", "easyrsa")

  os.chdir(os.path.join("certs", domain))

  c.run(" ".join([easyrsa_bin, "--batch build-server-full", name, "nopass"]))


@task(help={
  "name": "Name of the client cert",
  "domain": "Domain to create the cert under"
})
def generate_client(c, name, domain='default'):
  """
  Generates a client cert with the given name for the given domain.
  """
  
  easyrsa_bin = os.path.join(os.path.abspath(os.curdir), "tools", "easyrsa", "easyrsa")

  os.chdir(os.path.join("certs", domain))

  c.run(" ".join([easyrsa_bin, "--batch build-client-full", name, "nopass"]))

@task
def make_server_config(c, name="server", domain='default', output="", config="config.yml"):
  """
  Creates a directory containing all certs and configuration needed to start an openvpn server.
  """

  certs_dir = os.path.join("certs", domain)

  if not output:
    output = name

  # Read config
  with open(config) as f:
    conf = yaml.load(f)

  c.run("mkdir {}".format(output))

  # Copy certs
  c.run(" ".join(["cp", os.path.join(certs_dir, "pki", "ca.crt"), output]))
  c.run(" ".join(["cp", os.path.join(certs_dir, "pki", "issued", name + ".crt"), output]))
  c.run(" ".join(["cp", os.path.join(certs_dir, "pki", "private", name + ".key"), output]))
  c.run(" ".join(["cp", os.path.join(certs_dir, "ta.key"), output]))
  c.run(" ".join(["cp", os.path.join(certs_dir, "dh.pem"), output]))

  # Render config
  with open(os.path.join(current_dir, "vpn_config", "server.conf.j2")) as f:
    template = jinja2.Template(f.read())
  
  openvpn_conf = template.render(**{
    **conf,
    "server_name": name
  })

  with open(os.path.join(output, "openvpn.conf"), "w") as f:
    f.write(openvpn_conf)
  
  print("Wrote config to {}".format(output))

@task
def make_client_config(c, name, domain='default', output="", config="config.yml"):
  """
  Creates an ovpn file with the credendials needed to log in.
  """

  orig_dir = os.path.abspath(os.curdir)

  # Read config
  with open(config) as f:
    conf = yaml.load(f)

  # To the certs dir!
  os.chdir(os.path.join("certs", domain))
  if not output:
    output = name + ".ovpn"
  
  # Gather info
  with open("pki/ca.crt") as f:
    ca_cert = f.read()
  with open("ta.key") as f:
    ta_key = f.read()
  with open("pki/issued/{}.crt".format(name)) as f:
    client_cert = f.read()
  with open("pki/private/{}.key".format(name)) as f: 
    client_key = f.read()
  
  # Render ovpn
  with open(os.path.join(current_dir, "vpn_config", "client.ovpn.j2")) as f:
    template = jinja2.Template(f.read())
  
  ovpn = template.render(**{
    **conf,
    "ca_cert": ca_cert,
    "ta_key": ta_key,
    "client_cert": client_cert,
    "client_key": client_key
  })

  os.chdir(orig_dir)
  with open(output, "w") as f:
    f.write(ovpn)
  
  print("Wrote config to {}".format(output))


@task(help={
  "hosts": "Adds a host to the list of hosts to deploy to",
  "config": "Path to the config file. Defaults to ../config.yml"
})
def deploy(c, hosts=[], server_name='server', domain='default', config='config.yml'):
  if not len(hosts) > 0:
    print("At least one host needed to deploy", file=sys.stderr)
    return

  # Set up the command
  command = ["ansible-playbook"]

  # Append hosts to deploy to
  command += ["-i", ",".join(hosts)+',']

  # Append config
  #command += ["-vvv"]
  command += ["-e", "@" + config]
  command += ["-e", "server_name=" + server_name]
  command += ["-e", "domain_name=" + domain]
  command += ["-e", "working_dir=" + os.path.abspath(os.curdir)]
  

  # Playbooks
  command += [os.path.join(current_dir,"ansible/deploy.yml")]

  # Become pass
  command += ["--ask-become-pass"]

  c.run(' '.join(command))
  


@task(help={
  "device": "Path to the device to mount"
})
def mount(c, device):
  """
  Mounts the given device to the certs directory. This allows certs to easily be stored externally
  """
  c.run(" ".join("mount", "", device, "certs"))